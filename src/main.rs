//
// selfplay: UCI Self Play Tool
// src/main.rs: Main entry point
//
// Copyright (c) 2021 Ali Polatel <alip@exherbo.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::collections::HashMap;
use std::io::{BufRead, Write};

use signal_hook::consts::TERM_SIGNALS;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;

use std::process::Command;

use glob::glob;
use rand::seq::IteratorRandom;

#[macro_use]
extern crate lazy_static;

use clap::{App, Arg};

use log::{info, warn};
use log::{Level, LevelFilter, Metadata, Record};
static CONSOLE_LOGGER: ConsoleLogger = ConsoleLogger;
struct ConsoleLogger;

use regex::Regex;
use shakmaty::{Chess, FromSetup, Position};
use shakmaty_syzygy::{Syzygy, SyzygyError, Tablebase, Wdl};

use selfplay::built_info;
use selfplay::uci;

const FEN_STARTING_POSITION: &str = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";

fn main() {
    if let Err(error) = log::set_logger(&CONSOLE_LOGGER) {
        eprintln!("failed to set up logger: {}", error);
        std::process::exit(1);
    }
    log::set_max_level(LevelFilter::Info);

    play::<Chess>();
}

fn play<S>()
where
    S: Position + FromSetup + Syzygy + Clone,
{
    let matches = App::new(built_info::PKG_NAME)
        .version(built_info::PKG_VERSION)
        .author(built_info::PKG_AUTHORS)
        .about(built_info::PKG_DESCRIPTION)
        .after_help("\
CONFIGURATION:
UCI options are read from ~/.<engine-name>.uci only once and from \
~/.<engine-name>-a.uci and ~/.<engine-name>-b.uci at the beginning of each \
playout. The first file is for both players, whereas the file *-a.uci is for \
Player A and the file *-b.uci is for Player B respectively. If these paths are \
directories with the same name rather than regular files, a random file with \
the `.uci' extension located directly under the respective directory is picked \
and used as the configuration file. The format of the files are lines with the \
format \"setoption name <name> value <value>\". Lines starting with the character \
`#' are ignored. The names and values of the UCI configure options are available \
in the output PGN via the EngineOpts, EngineOptsA and EngineOptsB headers.

PER-PLAYOUT COMMAND EXECUTION:
If --exec option is given with a command as argument, the given command is executed \
before each playout using `sh -c'. Playout is paused until the exit of the command. \
The command may use the SELF_ROUND environment variable to determine the current \
round."
        )
        .arg(
            Arg::with_name("debug")
                .help("Prints debug information verbosely")
                .long("debug")
        )
        .arg(
            Arg::with_name("quiet")
                .help("Prints errors and warnings only")
                .long("quiet")
        )
        .arg(
            Arg::with_name("trace")
                .help("Prints trace information verbosely")
                .long("trace"),
        )
        .arg(
            Arg::with_name("position")
                .default_value(FEN_STARTING_POSITION)
                .required(true)
                .help("Initial position with Forsyth-Edwards Notation")
                .short("p"),
        )
        .arg(
            Arg::with_name("count")
                .default_value("0")
                .required(true)
                .help("Count of playouts, 0 to play until interrupted")
                .short("c"),
        )
        .arg(
            Arg::with_name("engine")
                .default_value("stockfish")
                .required(true)
                .help("path to engine binary")
                .short("e"),
        )
        .arg(
            Arg::with_name("output")
                .default_value("selfplay.pgn")
                .required(true)
                .help("output PGN file")
                .short("o")
                .env("SELFPLAY_PGN")
        )
        .arg(
            Arg::with_name("movetime-a")
                .default_value("100")
                .required(true)
                .help("move time for player A in milliseconds")
                .short("a"),
        )
        .arg(
            Arg::with_name("movetime-b")
                .default_value("100")
                .required(true)
                .help("move time for player B in milliseconds")
                .short("b"),
        )
        .arg(
            Arg::with_name("steptime-a")
                .help("given <step:limit>, change movetime-a every move until limit is reached")
                .long("steptime-a")
                .takes_value(true)
        )
        .arg(
            Arg::with_name("steptime-b")
                .help("given <step:limit>, change movetime-b every move until limit is reached")
                .long("steptime-b")
                .takes_value(true)
        )
        .arg(
            Arg::with_name("chess960")
                .help("Chess960 mode")
                .short("9")
                .long("chess960"),
        )
        .arg(
            Arg::with_name("syzygy")
                .help("specify directories of Syzygy WDL tablebases up to 7 pieces")
                .short("s")
                .long("syzygy")
                .multiple(true)
                .takes_value(true)
                .env("SYZYGY_PATH")
        )
        .arg(
            Arg::with_name("draw")
                .help("claim draw if no checks or captures occurs for the given number of moves, 0 to disable")
                .short("d")
                .long("draw")
                .takes_value(true)
                .default_value("50")
        )
        .arg(
            Arg::with_name("repetition")
                .help("claim draw if the same position occurs for the given number of times, 0 to disable")
                .short("r")
                .long("repetition")
                .takes_value(true)
                .default_value("3")
        )
        .arg(
            Arg::with_name("exec")
                .help("execute given command before each playout and wait for completion")
                .long("exec")
                .takes_value(true)
                .value_name("COMMAND")
        )
        .get_matches();

    let opt_debug = matches.is_present("debug");
    let opt_quiet = matches.is_present("quiet");
    let opt_trace = matches.is_present("trace");
    if opt_debug && opt_quiet || opt_debug && opt_trace || opt_quiet && opt_trace {
        clap::Error::with_description(
            "Flags --debug, --quiet and --trace are mutually exclusive.",
            clap::ErrorKind::InvalidValue,
        )
        .exit();
    } else if opt_quiet {
        log::set_max_level(LevelFilter::Warn);
    } else if opt_debug {
        log::set_max_level(LevelFilter::Debug);
    } else if opt_trace {
        log::set_max_level(LevelFilter::Trace);
    }

    let opt_exec: String;
    if matches.is_present("exec") {
        opt_exec = matches.value_of("exec").unwrap().to_string();
    } else {
        opt_exec = String::from("");
    }

    let opt_chess960 = matches.is_present("chess960");
    let opt_position = matches.value_of("position").unwrap();

    let setup: shakmaty::fen::Fen = match opt_position.parse() {
        Ok(setup) => setup,
        Err(error) => {
            clap::Error::with_description(
                &format!("Invalid value `{}' for --position: {}", opt_position, error),
                clap::ErrorKind::InvalidValue,
            )
            .exit();
        }
    };
    let fullmove = u32::from(setup.fullmoves);

    let castling = if opt_chess960 {
        shakmaty::CastlingMode::Chess960
    } else {
        shakmaty::CastlingMode::Standard
    };
    let mut position: S = match setup.position(castling) {
        Ok(position) => position,
        Err(error) => {
            clap::Error::with_description(
                &format!("Invalid value `{}' for --position: {}", opt_position, error),
                clap::ErrorKind::InvalidValue,
            )
            .exit();
        }
    };

    let opt_count = matches.value_of("count").unwrap();
    let opt_count = match opt_count.parse::<u64>() {
        Ok(opt_count) => opt_count,
        Err(error) => {
            clap::Error::with_description(
                &format!("Invalid value `{}' for --count: {}", opt_count, error),
                clap::ErrorKind::InvalidValue,
            )
            .exit();
        }
    };

    let opt_draw = matches.value_of("draw").unwrap();
    let opt_draw = match opt_draw.parse::<u32>() {
        Ok(opt_draw) => opt_draw * 2, /* convert to half moves */
        Err(error) => {
            clap::Error::with_description(
                &format!("Invalid value `{}' for --draw: {}", opt_draw, error),
                clap::ErrorKind::InvalidValue,
            )
            .exit();
        }
    };

    let opt_repetition = matches.value_of("repetition").unwrap();
    let opt_repetition = match opt_repetition.parse::<u8>() {
        Ok(opt_repetition) => opt_repetition,
        Err(error) => {
            clap::Error::with_description(
                &format!(
                    "Invalid value `{}' for --repetition: {}",
                    opt_repetition, error
                ),
                clap::ErrorKind::InvalidValue,
            )
            .exit();
        }
    };

    let movetime_a = matches.value_of("movetime-a").unwrap();
    let movetime_a = match movetime_a.parse::<u32>() {
        Ok(movetime_a) => movetime_a,
        Err(error) => {
            clap::Error::with_description(
                &format!("Invalid value `{}' for --movetime-a: {}", movetime_a, error),
                clap::ErrorKind::InvalidValue,
            )
            .exit();
        }
    };
    let movetime_b = matches.value_of("movetime-b").unwrap();
    let movetime_b = match movetime_b.parse::<u32>() {
        Ok(movetime_b) => movetime_b,
        Err(error) => {
            clap::Error::with_description(
                &format!("Invalid value `{}' for --movetime-a: {}", movetime_b, error),
                clap::ErrorKind::InvalidValue,
            )
            .exit();
        }
    };

    let mut steptime_a: [u32; 2] = [0, 0];
    if matches.is_present("steptime-a") {
        let mut split = matches.value_of("steptime-a").unwrap().split(':');
        for idx in 0..2 {
            steptime_a[idx] = split
                .next()
                .unwrap()
                .parse::<u32>()
                .expect("invalid argument to --steptime-b");

            if steptime_a[idx] == 0 {
                clap::Error::with_description(
                    "Invalid value for --steptime-a: zero",
                    clap::ErrorKind::InvalidValue,
                )
                .exit();
            }
        }

        if movetime_a == steptime_a[1] {
            clap::Error::with_description(
                "Invalid value for --steptime-a: limit equals movetime-a",
                clap::ErrorKind::InvalidValue,
            )
            .exit();
        }
    }

    let mut steptime_b: [u32; 2] = [0, 0];
    if matches.is_present("steptime-b") {
        let mut split = matches.value_of("steptime-b").unwrap().split(':');
        for idx in 0..2 {
            steptime_b[idx] = split
                .next()
                .unwrap()
                .parse::<u32>()
                .expect("invalid argument to --steptime-b");

            if steptime_b[idx] == 0 {
                clap::Error::with_description(
                    "Invalid value for --steptime-b: zero",
                    clap::ErrorKind::InvalidValue,
                )
                .exit();
            }
        }

        if movetime_a == steptime_a[1] {
            clap::Error::with_description(
                "Invalid value for --steptime-b: limit equals movetime-b",
                clap::ErrorKind::InvalidValue,
            )
            .exit();
        }
    }

    let opt_output = matches.value_of("output").unwrap();
    let mut output = match std::fs::OpenOptions::new()
        .append(true)
        .create(true)
        .open(opt_output)
    {
        Ok(output) => output,
        Err(error) => {
            warn!("error opening file `{}' for writing: {}", opt_output, error);
            std::process::exit(1);
        }
    };

    let opt_engine = matches.value_of("engine").unwrap();
    let mut engine = uci::Engine::new(opt_engine).unwrap();

    let mut engine_opts_main = Vec::<(String, String)>::new();
    let mut engine_opts_a = Vec::<(String, String)>::new();
    let mut engine_opts_b = Vec::<(String, String)>::new();

    let syzygy: Option<Tablebase<S>> = if matches.is_present("syzygy") {
        let mut tb = Tablebase::new();
        for path in matches.values_of("syzygy").unwrap() {
            if let Err(error) = tb.add_directory(path) {
                clap::Error::with_description(
                    &format!("Invalid value `{}' for --syzygy: {}", path, error),
                    clap::ErrorKind::InvalidValue,
                )
                .exit();
            }
        }
        Some(tb)
    } else {
        None
    };

    let hostname = match hostname::get() {
        Ok(hostname) =>
        /* OsString -> String */
        {
            match hostname.into_string() {
                Ok(hostname) => hostname,
                Err(error) => {
                    warn!("error getting hostname: {:?}", error);
                    String::from("?")
                }
            }
        }
        Err(error) => {
            warn!("error getting hostname: {}", error);
            String::from("?")
        }
    };

    let playout_id = uuid::Uuid::new_v4().to_string();

    let mut round = 1;
    let mut pgn_header = format!(
        "
[Event \"Self Play {}\"]
[Site \"{}\"]
[White \"A\"]
[Black \"B\"]
",
        playout_id, hostname
    );

    if opt_position != FEN_STARTING_POSITION {
        pgn_header.push_str(&format!("[FEN \"{}\"]\n[SetUp \"1\"]\n", opt_position));
    }

    if opt_chess960 {
        pgn_header.push_str("[Variant \"chess960\"]\n");
    }

    if movetime_a == movetime_b {
        pgn_header.push_str(&format!("[TimeControl \"{}\"]", movetime_a));
    } else {
        pgn_header.push_str(&format!("[WhiteTimeControl \"{}\"]", movetime_a));
        pgn_header.push_str(&format!("[BlackTimeControl \"{}\"]", movetime_b));
    }
    pgn_header.push('\n');

    if steptime_a[0] > 0 {
        pgn_header.push_str(&format!("[WhiteTimeStep \"{}:{}\"]\n", steptime_a[0], steptime_a[1]));
    }
    if steptime_b[0] > 0 {
        pgn_header.push_str(&format!("[BlackTimeStep \"{}:{}\"]\n", steptime_b[0], steptime_b[1]));
    }

    if let Some(ref engine_name) = engine.name {
        pgn_header.push_str(&format!("[EngineName \"{}\"]\n", engine_name));
    }
    parse_config(&opt_engine, '\0', &mut engine_opts_main);
    if !engine_opts_main.is_empty() {
        pgn_header.push_str(&format!(
            "[EngineOpts \"{}\"]\n",
            engine_opt_header(&engine_opts_main)
        ));
        apply_config(&engine, &engine_opts_main);
    }

    let exit = Arc::new(AtomicBool::new(false));
    for sig in TERM_SIGNALS {
        signal_hook::flag::register_conditional_shutdown(*sig, 1, Arc::clone(&exit)).unwrap();
        signal_hook::flag::register(*sig, Arc::clone(&exit)).unwrap();
    }

    let mut repetition = HashMap::<u64, u8>::new();
    loop {
        if exit.load(Ordering::Relaxed) {
            info!("SIG");
            break;
        } else {
            info!("OUT {}", round);
        }

        if !opt_exec.is_empty() {
            let status = match Command::new("sh")
                .arg("-c")
                .arg(&opt_exec)
                .env("SELF_ROUND", format!("{}", round))
                .status()
            {
                Ok(status) => status,
                Err(error) => {
                    warn!("error executing `{}' with `sh -c': {}", opt_exec, error);
                    std::process::exit(1);
                }
            };
            info!("EXE {}", status);
        }

        let mut game_header = String::from(&pgn_header);
        game_header.push_str(&format!("[Round \"{}\"]\n", round));

        engine_opts_a.clear();
        engine_opts_b.clear();
        parse_config(&opt_engine, 'a', &mut engine_opts_a);
        parse_config(&opt_engine, 'b', &mut engine_opts_b);

        if !engine_opts_a.is_empty() {
            game_header.push_str(&format!(
                "[EngineOptsA \"{}\"]\n",
                engine_opt_header(&engine_opts_a)
            ));
        }
        if !engine_opts_b.is_empty() {
            game_header.push_str(&format!(
                "[EngineOptsB \"{}\"]\n",
                engine_opt_header(&engine_opts_b)
            ));
        }

        engine.command("ucinewgame").unwrap();
        engine.set_position(opt_position).unwrap();

        let mut fmc = fullmove;
        let mut pgn = String::from("");
        let mut outcome = "?";
        if round > 1 {
            position = setup.position(castling).unwrap()
        }
        if opt_repetition > 0 {
            repetition.insert(selfplay::zobrist_hash(&position), 1);
        }

        let mut game_movetime_a = movetime_a;
        let mut game_movetime_b = movetime_b;
        'playout_game: loop {
            let wtm = position.turn() == shakmaty::Color::White;
            if wtm {
                engine = engine.movetime(game_movetime_a);
                apply_config(&engine, &engine_opts_a);

                /* TODO: deduplicate with the other arm of the branch. */
                if steptime_a[0] > 0 && game_movetime_a != steptime_a[1] {
                    match game_movetime_a > steptime_a[1] {
                        true => {
                            let n = game_movetime_a - steptime_a[0];
                            if n > 0 && n >= steptime_a[1] {
                                game_movetime_a = n;
                            } else { /* step done. */
                                steptime_a[0] = 0;
                            }
                        },
                        false => {
                            let n = game_movetime_a + steptime_a[0];
                            if n > 0 && n <= steptime_a[1] {
                                game_movetime_a = n;
                            } else { /* step done. */
                                steptime_a[0] = 0;
                            }
                        }
                    }
                }
            } else {
                engine = engine.movetime(game_movetime_b);
                apply_config(&engine, &engine_opts_b);

                if steptime_b[0] > 0 && game_movetime_b != steptime_b[1] {
                    match game_movetime_b > steptime_b[1] {
                        true => {
                            let n = game_movetime_b - steptime_b[0];
                            if n > 0 && n >= steptime_b[1] {
                                game_movetime_b = n;
                            } else { /* step done. */
                                steptime_b[0] = 0;
                            }
                        },
                        false => {
                            let n = game_movetime_b + steptime_b[0];
                            if n > 0 && n <= steptime_b[1] {
                                game_movetime_b = n;
                            } else { /* step done. */
                                steptime_b[0] = 0;
                            }
                        }
                    }
                }
            }

            let next_move = engine.bestmove().expect("error reading best move from engine");
            let uci_move = match shakmaty::uci::Uci::from_ascii(next_move.as_bytes()) {
                Ok(uci_move) => uci_move,
                Err(error) => {
                    warn!("invalid move `{}' from engine: {}", next_move, error);
                    break 'playout_game;
                }
            };
            let uci_move = match uci_move.to_move(&position) {
                Ok(uci_move) => uci_move,
                Err(error) => {
                    warn!("invalid move `{}' from engine: {}", next_move, error);
                    break 'playout_game;
                }
            };

            info!("MOV {}/{}:{}", fmc, position.halfmoves(), next_move);
            if wtm {
                pgn.push_str(&format!("{}. ", fmc));
            } else if pgn.is_empty() {
                pgn.push_str(&format!("{}... ", fmc));
                fmc += 1;
            } else {
                fmc += 1;
            }
            pgn.push_str(&shakmaty::san::San::from_move(&position, &uci_move).to_string());
            pgn.push(' ');

            position.play_unchecked(&uci_move);

            /*
             * We set position early here to let the engine know that the
             * game is potentially decided.
             */
            let fen = shakmaty::fen::fen(&position);
            engine.set_position(&fen).unwrap();

            match position.outcome() {
                Some(shakmaty::Outcome::Draw) => {
                    info!("RES 1/2");
                    outcome = "1/2-1/2";
                    break;
                }
                Some(shakmaty::Outcome::Decisive {
                    winner: shakmaty::Color::White,
                }) => {
                    info!("RES 1-0");
                    outcome = "1-0";
                    break;
                }
                Some(shakmaty::Outcome::Decisive {
                    winner: shakmaty::Color::Black,
                }) => {
                    info!("RES 0-1");
                    outcome = "0-1";
                    break;
                }
                None => {
                    if opt_draw > 0 && opt_draw <= position.halfmoves() {
                        /*
                         * Step 1: Check 50-move rule.
                         */
                        info!("HMC {}", position.halfmoves());
                        outcome = "1/2-1/2";
                        break;
                    } else if opt_repetition > 0 {
                        /*
                         * Step 2: Repetition detection
                         */
                        let key = selfplay::zobrist_hash(&position);
                        let val = repetition.entry(key).and_modify(|v| *v += 1).or_insert(1);

                        if *val >= opt_repetition {
                            info!("REP {}", key);
                            info!("RES 1/2");
                            outcome = "1/2-1/2";
                            break;
                        }
                    }

                    if let Some(ref tb) = syzygy {
                        /*
                         * Step 3: Consult tablebases.
                         * Careful here, we played the move but haven't updated the
                         * wtm variable yet. Hence, the meaning of wtm is negated below.
                         */
                        outcome = match tb.probe_wdl(&position) {
                            Ok(Wdl::Loss) => {
                                info!("WDL {:?}", Wdl::Loss);
                                if wtm {
                                    "1-0"
                                } else {
                                    "0-1"
                                }
                            }
                            Ok(Wdl::Win) => {
                                info!("WDL {:?}", Wdl::Win);
                                if wtm {
                                    "0-1"
                                } else {
                                    "1-0"
                                }
                            }
                            /* BlessedLoss, CursedWin & Draw */
                            Ok(wdl) => {
                                info!("WDL {:?}", wdl);
                                "1/2-1/2"
                            }
                            Err(SyzygyError::Castling)
                            | Err(SyzygyError::TooManyPieces)
                            | Err(SyzygyError::MissingTable {
                                metric: _,
                                material: _,
                            }) => {
                                continue 'playout_game;
                            }
                            /* I/O error,
                             * Unexpected magic header bytes,
                             * Corrupted table
                             */
                            Err(error) => {
                                warn!("error probing wdl table for position `{}': {}", fen, error);
                                continue 'playout_game;
                            }
                        };
                        break;
                    }
                },
            }
        }

        let date = chrono::Local::now();
        let mut game = String::from("");
        game.push_str(&game_header);
        game.push_str(&format!("[Date \"{}\"]\n", date.format("%Y.%m.%d")));
        game.push_str(&format!("[Time \"{}\"]\n", date.format("%H:%M:%S")));
        game.push_str("[Result \"");
        game.push_str(outcome);
        game.push_str("\"]\n\n");
        game.push_str(&pgn);
        game.push_str(outcome);
        game.push('\n');

        if let Err(error) = output.write_all(game.as_bytes()) {
            warn!("error writing game to file `{}': {}", opt_output, error);
            warn!("writing PGN to standard output");
            println!("{}", game);
            println!();
        }

        repetition.clear();

        round += 1;
        if opt_count > 0 && round > opt_count {
            break;
        }
    }

    match engine.quit() {
        Ok(status) => {
            match status.code() {
                Some(code) => { info!("RET {}", code); }
                None => { info!("SIG"); }
            }
        },
        Err(error) => {
            warn!("error quitting engine: {}", error);
        }
    }
}

fn apply_config(engine: &uci::Engine, opt: &Vec<(String, String)>) {
    for engine_opt in opt.iter() {
        if let Err(error) = engine.set_option(&engine_opt.0, &engine_opt.1) {
            warn!(
                "error setting option `{}' to value `{}': {}",
                engine_opt.0, engine_opt.1, error
            );
            /*
            if opt.contains("Syzygy") {
                /* Workaround uci crate bug:
                 * > setoption name SyzygyPath value /syzygy
                 * < info string Found 562 tablebases
                 *
                 * uci-0.1.2 check in setoption:
                 * src/lib.rs:136 if error_msg.trim().is_empty() {
                 */
            }
            std::process::exit(1);
            */
        }
    }
}

fn parse_config(bin: &str, suffix: char, opt: &mut Vec<(String, String)>) {
    lazy_static! {
        // Note: the pattern below does not allow spaces in option names or values.
        static ref RE_SETOPTION: Regex =
            Regex::new(r"\Asetoption name (?P<name>.+) value (?P<value>.+)\z").unwrap();
    }

    let mut pieces = bin.rsplitn(2, |c| c == '/' || c == '\\');
    let name = match pieces.next() {
        None => match suffix {
            '\0' => format!("~/.{}.uci", bin),
            c => format!("~/.{}-{}.uci", bin, c),
        },
        Some(name) => match suffix {
            '\0' => format!("~/.{}.uci", name),
            c => format!("~/.{}-{}.uci", name, c),
        },
    };
    let mut config = shellexpand::tilde(&name).to_string();

    let metadata = match std::fs::metadata(&config) {
        Ok(metadata) => metadata,
        Err(error) => {
            warn!("error accessing `{}': {}", config, error);
            return;
        }
    };
    if metadata.is_dir() {
        /* pick a random UCI file from the directory */
        let pattern = format!("{}/*.uci", config);
        config = match glob(&pattern) {
            Ok(files) => {
                let mut rng = rand::thread_rng();
                match files
                    .map(|file| match file {
                        Ok(file) => match file.into_os_string().into_string() {
                            Ok(file) => file,
                            Err(error) => {
                                warn!("error retrieving path name in `{}': {:?}", config, error);
                                String::from("")
                            }
                        },
                        Err(error) => {
                            warn!(
                                "error during directory traversal in `{}': {}",
                                config, error
                            );
                            String::from("")
                        }
                    })
                    .choose(&mut rng)
                {
                    None => {
                        warn!("no config files found under `{}'", config);
                        return;
                    }
                    Some(file) => {
                        if file.is_empty() {
                            return;
                        }
                        file
                    }
                }
            }
            Err(error) => {
                warn!("error accessing `{}': {}", pattern, error);
                return;
            }
        };
    }

    let input = std::io::BufReader::new(match std::fs::File::open(&config) {
        Ok(file) => file,
        Err(error) => {
            warn!("failed to open config file `{}': {}", config, error);
            return; /* Continue without configuration. */
        }
    });

    for command in input.lines() {
        let command = match command {
            Ok(command) => command,
            Err(error) => {
                warn!("error reading config `{}': {}", config, error);
                break;
            }
        };

        if command.is_empty() {
            continue;
        }
        if command.chars().nth(0).unwrap() == '#' {
            continue;
        }

        let command = command.trim_end();
        match RE_SETOPTION.captures(command) {
            None => {
                warn!("error in config file `{}': `{}'", config, command);
                std::process::exit(1);
            }
            Some(captures) => {
                let key = captures.name("name").unwrap().as_str();
                let val = captures.name("value").unwrap().as_str();

                opt.push((key.to_string(), val.to_string()));
            }
        }
    }
}

#[allow(dead_code)]
fn wait_engine(engine: &uci::Engine) {
    loop {
        let reply = engine.command("isready").unwrap();
        if reply.starts_with("readyok") {
            return;
        }
        warn!("engine is not ready, replied: {:?}", reply);
        warn!("waiting for a second...");
        std::thread::sleep(std::time::Duration::new(1, 0));
    }
}

fn engine_opt_header(opt: &Vec<(String, String)>) -> String {
    let vec: Vec<String> = opt
        .iter()
        .filter(|v| !v.0.starts_with("Debug"))
        .map(|v| {
            if v.1 == "true" {
                format!("{}=t", v.0)
            } else if v.1 == "false" {
                format!("{}=f", v.0)
            } else {
                format!("{}={}", v.0, v.1)
            }
        })
        .collect();
    vec.join(";")
}

impl log::Log for ConsoleLogger {
    fn enabled(&self, metadata: &Metadata) -> bool {
        metadata.level() <= Level::Trace
    }

    fn log(&self, record: &Record) {
        if self.enabled(record.metadata()) {
            let level = record.level();
            if level == Level::Info {
                eprintln!("{}", record.args());
            } else {
                eprintln!("{} - {}", level, record.args());
            }
        }
    }

    fn flush(&self) {}
}

/*
 * TODO: write integration tests!
 *
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_foo() {
        test_bin::get_test_bin("selfplay")
                 .env("HOME", ".")
    }
}
*/
